#ifndef RBTREE_H
#define RBTREE_H

#include <algorithm>

template<typename Key, typename Value>
class RBTree
{
public:
    RBTree();
    ~RBTree();

    void insert(Key key, Value val);
    const Value &find(const Key &key) const;
    void erase(const Key &key);

    void clear();

    int size() const;

    struct Node
    {
        Node(Node *_parent = nullptr) : parent(_parent) { }

        enum Color
        {
            Red   = 0,
            Black = 1
        };

        Node *left;
        Node *right;

        Node *parent;

        Color color;

        Key key;
        Value value;
    };

    inline bool isLeaf(Node *node) const
    {
        return node == _leaf;
    }

    inline Node *_getRoot() const
    {
        return mRoot;
    }

    int getDepth() const
    {
        return _getDepth(mRoot);
    }

    void getWidth(float &left, float &right, int nodeWidth)
    {
        return _getWidth(mRoot, left, right, nodeWidth);
    }

    void _getWidth(Node *node, float &left, float &right, int nodeWidth) const;

private:
    Node *_leaf;

    void _rotateLeft(Node *node);
    void _rotateRight(Node *node);

    void _insertFixup(Node *node);
    void _deleteFixup(Node *node);

    Node *_insertNode(Key key, Value val);
    void _deleteNode(Node *node);

    void _deleteRecursive(Node *&node);
    int _size(Node *node) const;

    Node *_findNode(const Key &key) const;

    int _getDepth(Node *root) const
    {
        if (!root || isLeaf(root))
        {
            return 0;
        }

        return 1 + std::max(_getDepth(root->left), _getDepth(root->right));
    }

    Node *mRoot;
};

template<typename K, typename V>
RBTree<K, V>::RBTree()
{
    _leaf = new Node;

    _leaf->left   = _leaf;
    _leaf->right  = _leaf;
    _leaf->parent = nullptr;
    _leaf->color  = Node::Black;

    mRoot = _leaf;
}

template<typename K, typename V>
RBTree<K, V>::~RBTree()
{
    clear();

    delete _leaf;
}

/// ----------------------------------------------------- ///

template<typename K, typename V>
void RBTree<K, V>::insert(K key, V val)
{
    (void)_insertNode(key, val);
}

template<typename K, typename V>
void RBTree<K, V>::erase(const K &key)
{
    _deleteNode(_findNode(key));
}


template<typename K, typename V>
const V &RBTree<K, V>::find(const K &key) const
{
    auto *node = _findNode(key);
    if (node)
    {
        return node->value;
    }

    const static V stub;
    return stub;
}

template<typename K, typename V>
void RBTree<K, V>::clear()
{
    _deleteRecursive(mRoot);
}

template<typename K, typename V>
void RBTree<K, V>::_deleteRecursive(Node *&node)
{
    if (!node)
    {
        return;
    }

    if (node != _leaf)
    {
        _deleteRecursive(node->left);
        _deleteRecursive(node->right);

        delete node;
    }

    node = _leaf;
}

template<typename K, typename V>
int RBTree<K, V>::size() const
{
    return _size(mRoot);
}

template<typename K, typename V>
int RBTree<K, V>::_size(Node *node) const
{
    if (node == _leaf)
    {
       return 0;
    }

    return _size(node->left) + _size(node->right) + 1;
}

/// ----------------------------------------------------- ///

template<typename K, typename V>
void RBTree<K, V>::_rotateLeft(Node *node)
{
    Node *y = node->right;

    /* establish node->right link */
    node->right = y->left;
    if (y->left != _leaf)
    {
        y->left->parent = node;
    }

    /* establish y->parent link */
    if (y != _leaf)
    {
        y->parent = node->parent;
    }

    if (node->parent)
    {
        if (node == node->parent->left)
        {
            node->parent->left = y;
        }
        else
        {
            node->parent->right = y;
        }
    }
    else
    {
        mRoot = y;
    }

    /* link node and y */
    y->left = node;
    if (node != _leaf)
    {
        node->parent = y;
    }
}

template<typename K, typename V>
void RBTree<K, V>::_rotateRight(Node *node)
{
    Node *y = node->left;

    /* establish node->left link */
    node->left = y->right;
    if (y->right != _leaf)
    {
        y->right->parent = node;
    }

    /* establish y->parent link */
    if (y != _leaf)
    {
        y->parent = node->parent;
    }

    if (node->parent)
    {
        if (node == node->parent->right)
        {
            node->parent->right = y;
        }
        else
        {
            node->parent->left = y;
        }
    }
    else
    {
        mRoot = y;
    }

    /* link node and y */
    y->right = node;
    if (node != _leaf)
    {
        node->parent = y;
    }
}

/// ----------------------------------------------------- ///

template<typename K, typename V>
void RBTree<K, V>::_insertFixup(Node *x)
{
    /* check Red-Black properties */
    while (x != mRoot && x->parent->color == Node::Red) {
        /* we have a violation */
        if (x->parent == x->parent->parent->left)
        {
            Node *y = x->parent->parent->right;
            if (y->color == Node::Red)
            {

                /* uncle is RED */
                x->parent->color = Node::Black;
                y->color = Node::Black;
                x->parent->parent->color = Node::Red;
                x = x->parent->parent;
            }
            else
            {
                /* uncle is BLACK */
                if (x == x->parent->right) {
                    /* make x a left child */
                    x = x->parent;
                    _rotateLeft(x);
                }

                /* recolor and rotate */
                x->parent->color = Node::Black;
                x->parent->parent->color = Node::Red;
                _rotateRight(x->parent->parent);
            }
        }
        else
        {
            /* mirror image of above code */
            Node *y = x->parent->parent->left;
            if (y->color == Node::Red) {

                /* uncle is RED */
                x->parent->color = Node::Black;
                y->color = Node::Black;
                x->parent->parent->color = Node::Red;
                x = x->parent->parent;
            }
            else
            {
                /* uncle is BLACK */
                if (x == x->parent->left)
                {
                    x = x->parent;
                    _rotateRight(x);
                }
                x->parent->color = Node::Black;
                x->parent->parent->color = Node::Red;
                _rotateLeft(x->parent->parent);
            }
        }
    }

    mRoot->color = Node::Black;
}

template<typename K, typename V>
typename RBTree<K, V>::Node *RBTree<K, V>::_insertNode(K key, V val)
{
    /* find where node belongs */
    Node *current = mRoot;
    Node *parent = nullptr;
    while (current != _leaf)
    {
        if (key == current->key)
        {
            return current;
        }
        parent = current;
        current = key < current->key ? current->left : current->right;
    }

    /* setup new node */
    Node *newNode = new Node;

    newNode->key    = key;
    newNode->value  = val;
    newNode->parent = parent;
    newNode->left   = _leaf;
    newNode->right  = _leaf;
    newNode->color  = Node::Red;

    /* insert node in tree */
    if (parent)
    {
        if (key < parent->key)
        {
            parent->left = newNode;
        }
        else
        {
            parent->right = newNode;
        }
    }
    else
    {
        mRoot = newNode;
    }

    _insertFixup(newNode);
    return newNode;
}

/// ----------------------------------------------------- ///

template<typename K, typename V>
void RBTree<K, V>::_deleteFixup(Node *x)
{
    while (x != mRoot && x->color == Node::Black)
    {
        if (x == x->parent->left)
        {
            Node *w = x->parent->right;

            if (w->color == Node::Red)
            {
                w->color = Node::Black;
                x->parent->color = Node::Red;
                _rotateLeft(x->parent);
                w = x->parent->right;
            }

            if (w->left->color == Node::Black && w->right->color == Node::Black)
            {
                w->color = Node::Red;
                x = x->parent;
            }
            else
            {
                if (w->right->color == Node::Black)
                {
                    w->left->color = Node::Black;
                    w->color = Node::Red;
                    _rotateRight(w);
                    w = x->parent->right;
                }

                w->color = x->parent->color;
                x->parent->color = Node::Black;
                w->right->color = Node::Black;
                _rotateLeft(x->parent);
                x = mRoot;
            }
        }
        else
        {
            Node *w = x->parent->left;

            if (w->color == Node::Red)
            {
                w->color = Node::Black;
                x->parent->color = Node::Red;
                _rotateRight(x->parent);
                w = x->parent->left;
            }

            if (w->right->color == Node::Black && w->left->color == Node::Black)
            {
                w->color = Node::Red;
                x = x->parent;
            }
            else
            {
                if (w->left->color == Node::Black)
                {
                    w->right->color = Node::Black;
                    w->color = Node::Red;
                    _rotateLeft(w);
                    w = x->parent->left;
                }

                w->color = x->parent->color;
                x->parent->color = Node::Black;
                w->left->color = Node::Black;
                _rotateRight(x->parent);
                x = mRoot;
            }
        }
    }

    x->color = Node::Black;
}

template<typename K, typename V>
void RBTree<K, V>::_deleteNode(Node *node)
{
    if (!node || node == _leaf)
    {
        return;
    }

    Node *y = nullptr;
    if (node->left == _leaf || node->right == _leaf)
    {
        /* y has a _leaf node as a child */
        y = node;
    }
    else
    {
        /* find tree successor with a _leaf node as a child */
        y = node->right;
        while (y->left != _leaf) y = y->left;
    }

    /* x is y's only child */
    Node *x = nullptr;
    if (y->left != _leaf)
    {
        x = y->left;
    }
    else
    {
        x = y->right;
    }

    /* remove y from the parent chain */
    x->parent = y->parent;
    if (y->parent)
    {
        if (y == y->parent->left)
        {
            y->parent->left = x;
        }
        else
        {
            y->parent->right = x;
        }
    }
    else
    {
        mRoot = x;
    }

    if (y != node)
    {
        node->key = y->key;
        node->value = y->value;
    }


    if (y->color == Node::Black)
    {
        _deleteFixup(x);
    }

    delete y;
}

template<typename K, typename V>
typename RBTree<K, V>::Node *RBTree<K, V>::_findNode(const K &key) const
{
    Node *current = mRoot;
    while (current != _leaf)
    {
        if (current->key == key)
        {
            return current;
        }

        current = key < current->key ? current->left : current->right;
    }

    return nullptr;
}

/// ----------------------------------------------------- ///

template<typename K, typename V>
void RBTree<K, V>::_getWidth(Node *node, float &left, float &right, int nodeWidth) const
{
    left  = 0;
    right = 0;

    if (!node || node == _leaf)
    {
        left  = .5f;
        right = .5f;
        return;
    }

    float deepLeft  = 0.0f;
    float deepRight = 0.0f;


    _getWidth(node->left, deepLeft, deepRight, nodeWidth);
    left += std::max(deepLeft, deepRight) * 2.0f;

    _getWidth(node->right, deepLeft, deepRight, nodeWidth);
    right += std::max(deepLeft, deepRight) * 2.0f;

    if (node->left != _leaf && node->right != _leaf)
    {
        left  += .5f;
        right += .5f;
    }
}

#endif // RBTREE_H
